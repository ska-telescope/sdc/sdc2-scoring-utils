#!/bin/bash

# Set env vars for default local deployment if not set
export SDC_SCORE_API_URL="${SDC_SCORE_API_URL:=http://localhost:3000}"
export SDC_SCORE_AUTH_URL="${SDC_SCORE_AUTH_URL:=http://sdcss-keycloak:8080/auth/}"
export SDC2_SCORER_ADMIN_USER="${SDC2_SCORER_ADMIN_USER:=admin}"
export SDC2_SCORER_ADMIN_PASSWORD="${SDC2_SCORER_ADMIN_PASSWORD:=password}"

PYTHONPATH=./:./src/ python -m pytest \
    --cov=src \
    --cov-config=setup.cfg \
    --cov-report=term \
    --cov-report=xml:code-coverage-integrationtests.xml \
    --cov-report=html \
    tests/integrationtests/
