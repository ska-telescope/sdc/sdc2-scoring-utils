"""???."""
from datetime import datetime

import pytest


def get_name(test_type, identifier):
    """???."""
    # Create new group
    time_str = datetime.now().strftime("%d%m%yT%H.%M.%S")

    return "{}_{}_{}".format(test_type, identifier, time_str)


@pytest.fixture
def group_name():
    """???."""
    return get_name("group", "1")


@pytest.fixture
def user_name():
    """???."""
    return get_name("user", "1")


@pytest.fixture
def new_email():
    """???."""
    return "{}@email.com".format(get_name("email", "1"))
