"""???."""
import time

import pytest

from ska_sdc2_scoring_utils.admin_api import (
    group_add,
    group_delete,
    group_list,
    user_add,
    user_delete,
    user_list,
    submission_rm,
)
from ska_sdc2_scoring_utils.user_api import (
    create_submission,
    display_leaderboard,
    submission_info,
)


def generate_user(user_name, email, group_id):
    """???.

    Use pytest fixture for name, but group_id must be passed (provided by server).
    """
    return (
        "j",
        "smith",
        user_name,
        email,
        group_id,
        "password",
    )


def attempt_cleanup(submission_id="", user_id="", group_id=""):
    """???."""
    error_msg = ""
    if submission_id:
        try:
            submission_rm(
                submission_id,
                "1.dev",
            )
        except RuntimeError as err:
            error_msg += "Error removing submission {} ({}).\n".format(
                submission_id, err
            )
    if user_id:
        try:
            user_delete(user_id)
        except RuntimeError as err:
            error_msg += "Error removing user {} ({}).\n".format(user_id, err)
    if group_id:
        try:
            group_delete(group_id)
        except RuntimeError as err:
            error_msg += "Error removing group {} ({}). ".format(group_id, err)
    if error_msg:
        raise RuntimeError("Unable to complete cleanup: {}".format(error_msg))


class TestUsers:
    """???."""

    @pytest.mark.skip(reason="not sure this is safe to run")
    def test_add_rm_user(self, group_name, user_name, new_email):
        """???."""
        groups_1 = group_list()
        # Test users methods
        users_1 = user_list()

        new_group_data = group_add(group_name)

        new_user = generate_user(user_name, new_email, new_group_data["group_id"])
        new_user_data = user_add(*new_user)

        users_2 = user_list()
        assert len(users_2) == len(users_1) + 1
        assert any(user["username"] == new_user[2] for user in users_2)

        # Now delete user
        user_delete(new_user_data["user_id"])
        users_3 = user_list()
        assert len(users_3) == len(users_1)
        assert users_3 == users_1

        # Finally delete new group
        group_delete(new_group_data["group_id"])
        groups_2 = group_list()
        assert len(groups_2) == len(groups_1)
        assert groups_2 == groups_1

    @pytest.mark.skip(reason="not sure this is safe to run")
    def test_submissions(self, group_name, user_name, new_email):
        """???."""

        groups_1 = group_list()
        users_1 = user_list()

        new_group_data = group_add(group_name)

        new_user = generate_user(user_name, new_email, new_group_data["group_id"])
        new_user_data = user_add(*new_user)

        # Create submission
        submission_resp = create_submission(
            new_user[2], new_user[5], "./tests/data/submission1.cat", "1.dev"
        )

        # Retrieve score; the server will take a few moments to process the submission
        max_retries = 10
        for retry in range(max_retries):
            time.sleep(3)
            sub_contents = submission_info(submission_resp["submission_id"], "1.dev")
            if sub_contents["result_contents"] is not None:
                break
            # Add test failure condition if too many retries
            if retry == (max_retries - 1):
                attempt_cleanup(
                    submission_id=submission_resp["submission_id"],
                    user_id=new_user_data["user_id"],
                    group_id=new_group_data["group_id"],
                )
                raise RuntimeError(
                    "Unable to retrieve score. "
                    "Test group/user/submission data successfully removed."
                )

        assert all(
            result_key in sub_contents["result_contents"]
            for result_key in (
                "_value",
                "_train",
                "_detail",
                "_n_det",
                "_n_bad",
                "_n_match",
                "_n_false",
                "_score_det",
                "_acc_pc",
                "_scores_df",
                "_match_df",
            )
        )
        assert sub_contents["result_contents"]["_value"] == -1.0

        # Verify the new submission appears on the leaderboard
        leaderboard_2 = display_leaderboard("1.dev")
        assert any(entry["username"] == new_user[2] for entry in leaderboard_2)

        # Test the delete submission endpoint:
        submission_rm(
            submission_resp["submission_id"],
            "1.dev",
        )
        # Verify the new submission has been removed from the leaderboard
        leaderboard_3 = display_leaderboard("1.dev")
        if leaderboard_3 is not None:
            assert all(entry["username"] != new_user[2] for entry in leaderboard_3)

        # Now delete user
        user_delete(new_user_data["user_id"])
        users_2 = user_list()
        assert len(users_2) == len(users_1)
        assert users_2 == users_1

        # Finally delete new group
        group_delete(new_group_data["group_id"])
        groups_2 = group_list()
        assert len(groups_2) == len(groups_1)
        assert groups_2 == groups_1
