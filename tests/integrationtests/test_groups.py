"""???."""
import pytest

from ska_sdc2_scoring_utils.admin_api import group_add, group_delete, group_list


class TestGroups:
    """???."""

    @pytest.mark.skip(reason="not sure this is safe to run")
    def test_add_group(self, group_name):
        """???."""
        # Test groups methods
        groups_1 = group_list()
        assert isinstance(groups_1, list)

        new_group_data = group_add(group_name)

        groups_2 = group_list()
        assert len(groups_2) == len(groups_1) + 1
        assert any(group["group_name"] == group_name for group in groups_2)

        # Now delete new group
        group_delete(new_group_data["group_id"])
        groups_3 = group_list()
        assert len(groups_3) == len(groups_1)
        assert groups_3 == groups_1
