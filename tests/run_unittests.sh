#!/bin/bash

PYTHONPATH=./:./src/ python -m pytest \
    --cov=src \
    --cov-config=setup.cfg \
    --cov-report=term \
    --cov-report=xml:code-coverage.xml \
    --cov-report=html \
    tests/unittests/ 