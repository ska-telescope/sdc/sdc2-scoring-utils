"""Tests for SDC2 scoring utilities admin API methods."""
from unittest.mock import MagicMock, patch

import pytest
from keycloak.exceptions import KeycloakAuthenticationError

from ska_sdc2_scoring_utils import admin_api, api_common

from mock_responses import DUMMY_KEYCLOAK_TOKEN

DUMMY_USERS_RESPONSE = [
    {
        "id": "00000000-0000-0000-0000-000000000001",
        "first_name": "Test",
        "last_name": "User",
        "username": "test",
        "email": "test@skatelescope.org",
        "group_id": "00000000-0000-0000-0000-000000000001",
    }
]

DUMMY_GROUPS_RESPONSE = [
    {
        "id": "00000000-0000-0000-0000-000000000001",
        "group_name": "SKA Operations Group",
    },
    {"id": "403e3c48-7446-11eb-b6ac-32f5d6a1706f", "group_name": "test_1"},
    {"id": "270f9fe2-76a2-11eb-b6ac-32f5d6a1706f", "group_name": "rmb_test_1"},
]

DUMMY_GROUP_NAME = "test_group_1"
DUMMY_UUID = "403e3c48-7446-11eb-b6ac-32f5d6a1706f"


# pylint: disable=too-few-public-methods
class MockResponse:
    """Mock requests response object."""

    def __init__(self, json_data, status_code):
        """Construct the mock response."""
        self.json_data = json_data
        self.status_code = status_code
        self.reason = "reason"

    def json(self):
        """Return json response."""
        return self.json_data


def fake_get_config(endpoint):
    """Mock the the get_config() method.

    get_config() is a method that returns json data in the form of a python
    dictionary from a specified scoring service API endpoint.
    """
    if endpoint == "/users":
        user_uuid = "72728035-7e2c-40b6-b454-5e553eecb30a"
        group_uuid = "00000000-0000-0000-0000-000000000001"
        config = [
            [
                user_uuid,
                "first_name_1",
                "last_name_1",
                "user_name_1",
                "email_1",
                group_uuid,
            ]
        ]
    else:
        raise ValueError("Invalid endpoint")
    return config


def test_get_token_success():
    """."""
    with patch(
        "keycloak.KeycloakOpenID.token",
        MagicMock(return_value=DUMMY_KEYCLOAK_TOKEN),
    ) as _:
        token = api_common.get_token("user", "password")
        assert isinstance(token, dict)


def test_get_token_failure():
    """."""
    with pytest.raises(KeycloakAuthenticationError) as pytest_wrapped_e:
        api_common.get_token("user", "password")
    assert pytest_wrapped_e.type == KeycloakAuthenticationError


def test_get_group():
    """Check behaviour of get_group() method.

    This method gets the Group UUID and name when provided with either one
    of these. This function also minimally matches to the id or name.
    """
    # Note: In the following patch, we must point to the admin_api variable 'sdcss_get'
    # see https://stackoverflow.com/a/51615587/12126644
    with patch(
        "ska_sdc2_scoring_utils.admin_api.sdcss_get",
        MagicMock(return_value=DUMMY_GROUPS_RESPONSE),
    ):
        # Success
        for group_id in ("test_1", "t", "4"):
            uuid, name = admin_api.get_group(group_id)
            assert uuid == DUMMY_GROUPS_RESPONSE[1]["id"]
            assert name == DUMMY_GROUPS_RESPONSE[1]["group_name"]
        for group_id in ("rmb_test_1", "rmb", "27"):
            uuid, name = admin_api.get_group(group_id)
            assert uuid == DUMMY_GROUPS_RESPONSE[2]["id"]
            assert name == DUMMY_GROUPS_RESPONSE[2]["group_name"]

        # Failure
        with pytest.raises(ValueError) as pytest_wrapped_e:
            uuid, name = admin_api.get_group("")
        assert pytest_wrapped_e.type == ValueError
        print(pytest_wrapped_e.value)
        assert str(pytest_wrapped_e.value) == "Empty group identifier specified."


def test_get_user_uuid():
    """Test behaviour of method to get a users uuid from their username."""
    with patch(
        "ska_sdc2_scoring_utils.admin_api.sdcss_get",
        MagicMock(return_value=DUMMY_USERS_RESPONSE),
    ):
        # Success
        user_uuid_1 = admin_api.get_uuid_for_username("test")
        assert user_uuid_1 == "00000000-0000-0000-0000-000000000001"

        # Failure
        user_uuid_2 = admin_api.get_uuid_for_username("")
        assert user_uuid_2 is None


def test_get_user():
    """Test behaviour of getting list of users in a group."""
    with patch(
        "ska_sdc2_scoring_utils.admin_api.sdcss_get",
        MagicMock(return_value=DUMMY_USERS_RESPONSE),
    ):
        # Success
        user_match = admin_api.get_user("00000000-0000-0000-0000-000000000001")
        assert isinstance(user_match, dict)
        assert user_match == DUMMY_USERS_RESPONSE[0]

        # Failure
        with pytest.raises(ValueError) as pytest_wrapped_e:
            admin_api.get_user("")
        assert pytest_wrapped_e.type == ValueError
        assert str(pytest_wrapped_e.value) == "Empty user identifier specified."


def test_user_list():
    """Test behaviour of getting list of users."""
    with patch(
        "ska_sdc2_scoring_utils.admin_api.sdcss_get",
        MagicMock(side_effect=[DUMMY_USERS_RESPONSE, DUMMY_GROUPS_RESPONSE]),
    ):
        # Passes if no error thrown
        users = admin_api.user_list()
        assert users == DUMMY_USERS_RESPONSE


def test_user_add():
    """Test behaviour of adding a user."""
    with patch(
        "ska_sdc2_scoring_utils.admin_api.get_group",
        MagicMock(return_value=(DUMMY_UUID, DUMMY_GROUP_NAME)),
    ):
        # Behaviour when the API responds to notify that it has created the user.
        expected_resp_data = {"user_id": "mock_uuid"}
        with patch(
            "requests.post",
            MagicMock(return_value=MockResponse(expected_resp_data, 200)),
        ):
            # Passes if no error thrown
            user_data = admin_api.user_add(
                "firstname", "surname", "user_1", "email", "group_id", "password"
            )
            assert user_data == expected_resp_data

        # Behaviour when the API responds with a failure message.
        with patch(
            "requests.post",
            MagicMock(
                return_value=MockResponse({"detail": "add_user() request failed."}, 400)
            ),
        ):
            with pytest.raises(RuntimeError) as pytest_wrapped_e:
                admin_api.user_add(
                    "firstname", "surname", "user_1", "password", "email", "group_name"
                )
            assert str(pytest_wrapped_e.value) == (
                "POST /users failed with status 400. "
                "{'detail': 'add_user() request failed.'}"
            )


def test_user_delete():
    """Test behaviour of deleting a user."""
    with patch(
        "ska_sdc2_scoring_utils.admin_api.get_user",
        MagicMock(return_value=DUMMY_USERS_RESPONSE[0]),
    ):
        with patch(
            "ska_sdc2_scoring_utils.admin_api.sdcss_delete",
            MagicMock(return_value={}),
        ):
            # Passes if no error thrown
            admin_api.user_delete("user_1")
        with patch(
            "requests.delete",
            MagicMock(
                return_value=MockResponse(
                    {"detail": "delete_user() request failed."}, 400
                )
            ),
        ):
            with pytest.raises(RuntimeError) as pytest_wrapped_e:
                admin_api.user_delete("user_1")
            assert str(pytest_wrapped_e.value) == (
                "DELETE /users failed with status 400. "
                "{'detail': 'delete_user() request failed.'}"
            )


def test_group_list():
    """Test behaviour of getting the list of groups."""
    # get_config will get called once to get groups and then once
    # per group (to get users) - specify corresponding side_effects
    with patch(
        "ska_sdc2_scoring_utils.admin_api.sdcss_get",
        MagicMock(side_effect=[DUMMY_GROUPS_RESPONSE, DUMMY_USERS_RESPONSE]),
    ):
        # Passes if no error thrown
        groups = admin_api.group_list(show_users=True)
        assert all(groups[i] in DUMMY_GROUPS_RESPONSE for i in range(len(groups)))


def test_group_add():
    """Test behaviour of adding a group."""
    with patch(
        "ska_sdc2_scoring_utils.admin_api.sdcss_get",
        MagicMock(return_value=DUMMY_GROUPS_RESPONSE),
    ):
        # Success
        expected_resp_data = {"group_id": "mock_uuid"}
        with patch(
            "requests.post",
            MagicMock(return_value=MockResponse(expected_resp_data, 200)),
        ):
            # Passes if no error thrown
            group_data = admin_api.group_add("new_group_2")
            assert group_data == expected_resp_data

        # Failure
        api_error_response = dict(detail="add_group() request failed.")
        with patch(
            "requests.post",
            MagicMock(return_value=MockResponse(api_error_response, 400)),
        ):
            with pytest.raises(RuntimeError) as pytest_wrapped_e:
                admin_api.group_add("new_group_2")
            assert str(pytest_wrapped_e.value) == (
                "POST /groups failed with status 400. "
                "{'detail': 'add_group() request failed.'}"
            )

        # Already exists
        try:
            admin_api.group_add("test_1")
        except ValueError as error:
            assert str(error).startswith("Failed to add group")
            assert "group already exists!" in str(error)


def test_group_delete():
    """Test behaviour of deleting a group."""
    with patch(
        "ska_sdc2_scoring_utils.admin_api.get_group",
        MagicMock(return_value=(DUMMY_UUID, DUMMY_GROUP_NAME)),
    ):
        # Success
        with patch(
            "requests.delete",
            MagicMock(return_value=MockResponse({}, 200)),
        ):
            # Passes if no error thrown
            admin_api.group_delete("group_1")

        # Failure
        with patch(
            "requests.delete",
            MagicMock(
                return_value=MockResponse(
                    {"detail": "delete_group() request failed."}, 400
                )
            ),
        ), pytest.raises(RuntimeError) as pytest_wrapped_e:
            admin_api.group_delete("group_1")
        assert pytest_wrapped_e.type == RuntimeError
        assert str(pytest_wrapped_e.value) == (
            "DELETE /groups failed with status 400. "
            "{'detail': 'delete_group() request failed.'}"
        )
