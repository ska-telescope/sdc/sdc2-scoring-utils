"""Tests for SDC2 scoring utilities user API methods."""
from unittest.mock import MagicMock, patch

from ska_sdc2_scoring_utils import user_api


DUMMY_LEADERBOARD = [
    {
        "username": "user_1",
        "submission_id": "uuid",
        "group_name": "group_1",
        "score": 100.0,
        "contents": {
            "_value": 100.0,
            "_train": False,
            "_detail": False,
            "_n_det": 100,
            "_n_bad": 2,
            "_n_match": 0,
            "_n_false": 100,
            "_score_det": 0.0,
            "_acc_pc": 0.0,
        },
        "submitted_date": "2021-04-13T17:22:11.236151",
        "index": 0,
    },
    {
        "username": "user_2",
        "submission_id": "uuid",
        "group_name": "group_2",
        "score": 50.0,
        "contents": {
            "_value": 50.0,
            "_train": False,
            "_detail": False,
            "_n_det": 50,
            "_n_bad": 1,
            "_n_match": 0,
            "_n_false": 50,
            "_score_det": 0.0,
            "_acc_pc": 0.0,
        },
        "submitted_date": "2021-04-13T17:23:14.324893",
        "index": 1,
    },
]


def test_get_leaderboard():
    """
    Test getting the leaderboard using the get_config() method.
    """
    with patch(
        "ska_sdc2_scoring_utils.user_api.sdcss_get",
        MagicMock(return_value=DUMMY_LEADERBOARD),
    ) as _:
        leaderboard = user_api.display_leaderboard("1")
        assert leaderboard == DUMMY_LEADERBOARD
