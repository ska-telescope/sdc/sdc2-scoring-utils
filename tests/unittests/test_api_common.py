"""Tests for SDC2 scoring utilities common utility methods.

TODO(BMo): Convert to BDD style!
"""
from unittest.mock import MagicMock, patch

import pytest
from keycloak.exceptions import KeycloakAuthenticationError
from ska_sdc2_scoring_utils import api_common

from mock_responses import DUMMY_KEYCLOAK_TOKEN


def fake_sdcss_get(endpoint):
    """Mock the the sdcss_get() method.

    sdcss_get() is a method that returns json data in the form of a python
    dictionary from a specified scoring service API endpoint.
    """
    if endpoint == "/users":
        user_uuid = "72728035-7e2c-40b6-b454-5e553eecb30a"
        group_uuid = "00000000-0000-0000-0000-000000000001"
        config = [
            [
                user_uuid,
                "first_name_1",
                "last_name_1",
                "user_name_1",
                "email_1",
                group_uuid,
            ]
        ]
    else:
        raise ValueError("Invalid endpoint")
    return config


def test_get_token_success():
    """."""
    with patch(
        "keycloak.KeycloakOpenID.token",
        MagicMock(return_value=DUMMY_KEYCLOAK_TOKEN),
    ) as _:
        token = api_common.get_token(username="user", password="passwd")
        assert isinstance(token, dict)


def test_get_token_failure():
    """."""
    with pytest.raises(KeycloakAuthenticationError) as pytest_wrapped_e:
        api_common.get_token(username="", password="")
    assert pytest_wrapped_e.type == KeycloakAuthenticationError
