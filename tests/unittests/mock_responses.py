"""Mock Scoring service API responses."""

DUMMY_KEYCLOAK_TOKEN = {
    "access_token": "dummmy",
    "expires_in": 300,
    "refresh_expires_in": 1800,
    "refresh_token": "dummy",
    "token_type": "bearer",
    "not-before-policy": 0,
    "session_state": "uuid",
    "scope": "email profile",
}
