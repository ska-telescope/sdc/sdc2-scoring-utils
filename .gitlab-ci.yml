image: python:3.8

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - .cache/pip

stages:
  - lint
  - test
  - deploy

.requirements:
  &install_reqs # We don't need virtual environments so install directly
  - pip install pipenv && pipenv install --dev --system --deploy --ignore-pipfile

.reports: &ready_reports
  - mkdir -p build/reports/

pylint:
  stage: lint
  tags: [docker]
  before_script:
    - *install_reqs
    - *ready_reports
  script:
    - make lint
    - make lint-report
  after_script:
    - mv *.xml ./build/reports
  artifacts:
    paths:
      - build/
  when: always

test:
  stage: test
  tags: [docker]
  before_script:
    - *install_reqs
    - *ready_reports
  script:
    - make pytest-report
  after_script:
    - pip install anybadge
    - cov=$(grep -m 1 data-ratio htmlcov/index.html | cut -d">" -f2 | cut -d% -f1)
    - anybadge -l "coverage report" -v ${cov} -f ./build/reports/cov_report.svg coverage
    - mv *.xml ./build/reports
  coverage: '/^TOTAL.+?(\d+\%)$/'
  artifacts:
    name: "$CI_PROJECT_NAME-$CI_JOB_ID"
    paths:
      - build/
      - htmlcov/
    reports:
      junit: build/reports/unit-tests.xml
  when: always

pages:
  stage: deploy
  tags: [docker]
  dependencies:
    - test
    - pylint
  script:
    - mv htmlcov public
    - mv build/reports/cov_report.svg public
  artifacts:
    paths: [public/]
    expire_in: never

# Create Gitlab CI badges from CI metrics.
include:
  - project: "ska-telescope/templates-repository"
    file: "gitlab-ci/includes/post_step.yml"
