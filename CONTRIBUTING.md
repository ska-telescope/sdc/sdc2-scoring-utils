# Contributing

Contributions to `sdc2-scoring-utils` are welcome and greatly appreciated!

Please ensure you follow our [Code of Conduct](https://developer.skatelescope.org/en/latest/policies/code-of-conduct.html?highlight=code%20of%20conduct).

## Issue reporting

Not every contribution comes in the form of code. Submitting, confirming, and triaging
issues is an equally important task for the project. We use GitLab to track all project issues.
If you discover bugs, have ideas for improvements or new features, please start by
[opening an issue](https://gitlab.com/ska-telescope/sdc/sdc2-scoring-utils/-/issues) on 
this repository. We also use issues to centralize the discusion and agree 
on a plan of action before spending time and effort writing code.

### Submitting an issue

* Check that the issue has not already been reported.
* Be clear, consice, and precise.
* If reporting a bug, include details of your local set up that might be helpful
  in troubleshooting and detailed steps to reproduce the bug.

## Code Contributions

`sdc2-scoring-utils` follows a [forking workflow](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html), and we have a simple process.

1. Open an issue on the [project repository](https://gitlab.com/ska-telescope/sdc/sdc2-scoring-utils/-/issues), if appropriate.
2. Follow the [forking workflow](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork) sets:
3. Fork the project (<https://gitlab.com/ska-telescope/sdc/sdc2-scoring-utils/-/forks/new>)
4. Create your feature branch (`git checkout -b my-new-feature-or-bug-fix`)
5. Commit some changes (`git commit`)
6. Push your changes to the branch (`git push origin my-new-feature-or-bug-fix`)
7. Create a [Merge Request](https://docs.gitlab.com/ee/user/project/merge_requests/) following
the [merge request requirements](#Merge-request-requirements) and any instructions in the merge request template.
8. Participate in a code review with the project maintainers on the merge request.

### Merge request requirements

In order to ensure high quality for this project, we require that all merge
requests meet these specification:

* **Tests**: To ensure high quality code and protect against future regressions,
  we require tests for all new/changed functionality in `sdc2-scoring-utils`.
  Test positive and negaitive scenarios, try to break the new code now.
* **Green CI Tests**: We a GitLab CI pipeline to test all merge requests. We
  require these test runs to succeed on every merge request before being
  merged.
* **linting**: We require all code meets [PEP 8](https://www.python.org/dev/peps/pep-0008/)
 and all docstrings meet [PEP 257](https://www.python.org/dev/peps/pep-0257/).
 This can be checked by pylint, pycodstyle, flake8, pydocstyle and other tools and
 these checks form part of the CI the code past pass to be merged. We also recommend
 formatting code with [black](https://black.readthedocs.io/en/stable/?badge=stable).

### Code review process

Code review takes place in GitLab merge requests. Once you open a merge
request, project maintainers will review your code and respond to your
request with any feedback they might have. The process is as follows:

1. A review is required from at least one of the project maintainers.
2. Your change will be merged into the projects `master` branch, and all
[commits will be squashed](https://docs.gitlab.com/ee/user/project/merge_requests/squash_and_merge.html) during the merge.

## Releases

We release `sdc2-scoring-utils` as a [PyPI package](https://pypi.org/project/ska-sdc2-scoring-utils/).

We follow the [Semantic Versioning](https://semver.org/) standard with version numbers 
following the pattern `X.Y.Z`, where:

* `X` is a major release: has changes the are incompatible with prior major releases.
* `Y` is a minor release: adds new functionality and bug fixes in a backwards compatible manner.
* `Z` is a patch release: adds backwards compatible bug fixes.

*Exception: version < 1.0 may introduce backwards-incompatible changes in a minor release.*

### Pre-releases

Pre-releases are identified by a realease version followed by a pre-release segment 
(suffix) which consists of an alphabetical identifier, along with a non-negative
integer value.

* `X.Y.ZaN` indicates an **alpha** release.
* `X.Y.ZbN` indicates an **beta** release.
* `X.Y.ZrcN` indicates a **release candidate**.

Pre-releases are ordered by phase (alpha, beta, release candidate)
and then by numerical component.
