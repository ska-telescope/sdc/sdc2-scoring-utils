# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased] 2021-07-14

## Fixed

- Various linter and test issues.

## [1.0.0] 2021-04-12

### Changed

- Updated to SDC2 production scoring service API endpoints.
- Changed from using argparse to docopt

### Added

- Initial set of unit tests from upstream testing branch

## [0.2.2] 2021-02-06

### Fixed

- Fixed sphinx documentation not including the readme.
- Incorrect reporting of test coverage.

### Changed

- Removed pipenv run prefixes in makefile. The makes targets more flexible.

## [0.2.1] 2021-02-06

### Added

- Initial initial sphinx docs which are published to readthedocs.
- Added initial contributing guide (`CONTRIBUTING.md`)

### Changed

- Moved package version out of setup.py to `__version__.py` in the src folder.
  This allows it to be used by `setup.py` and documentation `conf.py` without
  setting the version in two places.

## [0.2.0rc1] 2021-01-31

### Changed

- Removed script folder and associated script files in favour of using `entrypoint` : `console_scripts` in `setup.py`. This change introduces two new
files in the source directory (`sdc2_score.py` and `sdc2_score_admin.py`) with
the functionality that was previously in these scripts.
- Improved working of top-level `README.md`.

### Added

- gitlab-ci script to run tests, linting, and coverage report and the mandated SKA
gitlab status badges.
