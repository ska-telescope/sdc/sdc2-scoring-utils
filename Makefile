PROJECT = sdc2-scoring-utils

all: help

######################################################################
# Python linting and testing
######################################################################

lint:  ## Run Pylint
	pylint --rcfile=.pylintrc --exit-zero -r n src/ska_sdc2_scoring_utils tests
.PHONY: lint

lint-report:  ## Lint and generate a report for the CI badgees.
	pylint --rcfile=.pylintrc \
		--exit-zero \
		-f pylint2junit.JunitReporter \
		src/ska_sdc2_scoring_utils \
		tests \
		| tee ./linting.xml > /dev/null
.PHONY: lint-report

pytest-quick:  ## Run pytest only.
	pytest -vv -s -x tests
.PHONY: pytest-quick

pytest-report:  ## Run pytest and generate reports for the CI badges.
	pytest -vv -s \
		--capture=no \
		--durations=5 \
		--pycodestyle \
		--pydocstyle \
		--cov=src \
		--cov-config=setup.cfg \
		--cov-report=term \
		--cov-report=html \
		--cov-report=xml:code-coverage.xml \
		--cov-branch \
		--cov-append \
		--no-cov-on-fail \
		--junitxml=unit-tests.xml \
		tests
.PHONY: pytest-report


pytest:  ## Run pytest with codestyle, docstule, and coverage.
	    pytest \
			-vv \
			-s \
			--capture=no \
			--pycodestyle \
			--pydocstyle \
			--cov=src \
			--cov-config=setup.cfg \
			--cov-report=term \
			--no-cov-on-fail \
			--cov-branch \
			--cov-append
.PHONY: quick-test



######################################################################
# Build python package
######################################################################

package:  ## Build python package.
	# https://python-packaging-tutorial.readthedocs.io/en/latest/uploading_pypi.html
	python setup.py sdist bdist_wheel
.PHONY: package


upload:  ## Upload python package to PyPi.
	twine upload --verbose --repository-url https://upload.pypi.org/legacy/ dist/*
.PHONY: upload


######################################################################
# Documentation
######################################################################

docs-autobuild:  ## Start sphinx-autobuild.
	sphinx-autobuild docs docs/_build/html
.PHONY: docs-autobuild

docs:  ## Build sphinx docs.
	sphinx-build docs/ docs/_build
.PHONY: docs

docker-test-env:  ## Create an interactive test enviroment inside a docker container.
	docker run --rm -it --entrypoint=/bin/bash -v "$(pwd)":/app python:3.8
.PHONY: docker-test-env

clean:  ## Clean out temporary files created by linting, tests, build etc.
	@rm -rf build dist
	@rm -rf src/ska_sdc2_scoring_utils.egg-info/
	@rm -rf htmlcov
	@rm -rf linting.xml
	@rm -rf coverage.xml
	@rm -rf code-coverage.xml
	@rm -rf unit-tests.xml
	@rm -rf .coverage
	@rm -rf docs/_build
	@rm -rf .tox/
	@rm -rf **/__pycache__
.PHONY: clean


help:  ## show this help.
	@echo "Make targets:"
	@grep -hE '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -hE '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " \?\= "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\#\#/  \#/'
.PHONY: help
